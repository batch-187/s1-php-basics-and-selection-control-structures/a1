<?php


function getFullAddress($country, $city, $province, $block){
	return "$block, $city, $province, $country";
}


function getLetterGrade($grade){
	if($grade >= 98){
     $letter = "A+";
  }else if($grade >= 95){
     $letter = "A";
  }else if($grade >= 92){
   $letter = "A-";
  }else if($grade >= 89){
   $letter = "B+";
  }else if($grade >= 86){
   $letter = "B";
  }else if($grade >= 83){
   $letter = "B-";
  }else if($grade >= 80){
   $letter = "C+";
  }else if($grade >= 77){
   $letter = "C";
  }else if($grade >= 75){
   $letter = "C-";
  }else{
   $letter = "D";
  }

   return "$grade is equivalent to $letter";
}

?>
